set LOCAL_DIR=%~dp0

"C:\Program Files (x86)\National Instruments\LabVIEW 2018\LabVIEW.exe" "%LOCAL_DIR%programmaticbuild.vi" -- "%LOCAL_DIR%PROJ\SRC\My Project.lvproj" "%LOCAL_DIR%buildnumber.txt"

IF NOT EXIST "%LOCAL_DIR%PROJ\SRC\pass.txt" (
    EXIT /B 1
) ELSE (
    EXIT /B 0
)