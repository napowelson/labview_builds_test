@echo off

REM Force Kill LabVIEW. Hard.
TASKLIST /FI "IMAGENAME eq labview.exe" | find /I /N "labview.exe">NUL
if "%ERRORLEVEL%"=="0" TASKKILL /F /IM labview.exe /T
REM Exit with no errors out of the script
EXIT 0
