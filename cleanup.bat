@echo on

IF EXIST builds rd /s /q builds
IF EXIST pass.txt DEL pass.txt
IF EXIST error_log.txt DEL error_log.txt

:: Clear Missing Tests and Tests Run files since Pre-Build Unit Tests Action.vi does not appear to do this yet - Alison 11/12/21
cd PROJ
IF EXIST "Missing Tests.txt" DEL "Missing Tests.txt"
IF EXIST "Tests Run.txt" DEL "Tests Run.txt"